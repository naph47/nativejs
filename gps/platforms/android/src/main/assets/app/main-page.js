var geo = require("nativescript-geolocation");
var page;

var observableModule = require("data/observable");

var location = = new Observable();

exports.pageLoaded = function (args) {
    page = args.object;

    // page.bindingContext = location;

    geo.isEnabled().then(function (rs) {
        console.log("enable " + rs);
    });
};

exports.enableGps = function () {
    geo.enableLocationRequest().then(function () {
        geo.isEnabled().then(function (rs) {
            console.log("enable " + rs);
            getLocation();
            if (rs) {
                getLocation();
            }
        });
    });

};

exports.checkLocation = function () {
    getLocation();
};

function getLocation() {
    return geo.getCurrentLocation({
        desiredAccuracy: 3,
        updateDistance: 10,
        maximumAge: 20000,
        timeout: 20000
    }).then(function (loc) {
        if (loc) {
            console.log("Current location is: " + JSON.stringify(loc));
            console.log(loc.latitude);
            // location.lat = loc.latitude;
            // location.lng = loc.longitude;
            // location.alt = loc.altitude;
            // location.set("lat", loc.latitude);
            location = = new Observable();
            location.set("lat", loc.latitude);
            // location = new observableModule.fromObject({
            //     lat: loc.latitude,
            //     lng: loc.longitude,
            //     alt: loc.altitude
            // });

            page.bindingContext = location;
        }
    }, function (e) {
        console.log("Error: " + e.message);
    });
}
